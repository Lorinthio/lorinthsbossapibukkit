package me.lorinth.bossapi.tasks;

import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.entity.LivingEntity;
import org.bukkit.scheduler.BukkitRunnable;

public class ParticleTask extends BukkitRunnable {

	private double count;
	private Particle particle;
	private LivingEntity ent;

	public ParticleTask(LivingEntity ent, Particle particle, double effectCount) {
		this.ent = ent;
		this.particle = particle;
		this.count = effectCount;
	}

	@Override
	public void run() {
		if (ent != null && !ent.isDead() && ent.isValid()) {
			World w = ent.getWorld();
			w.spawnParticle(particle, ent.getLocation(), (int) count, 0.1F, 0.1F, 0.1F, 0.05F);
		} else {
			this.cancel();
		}
	}
}