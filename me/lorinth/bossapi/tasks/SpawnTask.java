package me.lorinth.bossapi.tasks;

import me.lorinth.bossapi.BossApi;
import org.bukkit.entity.Creature;
import org.bukkit.entity.LivingEntity;
import org.bukkit.scheduler.BukkitRunnable;

import me.lorinth.bossapi.Spawner;

public class SpawnTask extends BukkitRunnable {

    private BossApi plugin;

    public SpawnTask(BossApi plugin) {
      this.plugin = plugin;
    }

  @Override
  public void run() {
    for (Spawner spawner : plugin.getSpawners()) {
      if (spawner.getBossInstance() == null) {
        if (System.currentTimeMillis() < spawner.getBossSpawnTime()) {
          continue;
        }
        if (!spawner.getChunk().isLoaded()) {
          continue;
        }
        spawner.killBoss();
        spawner.spawnBoss();
      } else {
        if (spawner.getRadius() == -1) {
          continue;
        }
        LivingEntity bossEntity = spawner.getBossInstance().getBossEntity();
        if (!bossEntity.isValid()) {
          spawner.setBossInstance(null);
          continue;
        }
        if (bossEntity.getLocation().distance(spawner.getLocation()) > spawner.getRadius()) {
          bossEntity.teleport(spawner.getLocation());
          spawner.setResetCount(spawner.getResetCount() + 1);
          if (bossEntity instanceof Creature) {
            ((Creature) bossEntity).setTarget(null);
          }
        }
      }
    }
  }
}
