package me.lorinth.bossapi.loot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class LootTable {

  private HashMap<String, DropSection> dropSections = new HashMap<>();
  private String name;

  public LootTable(String name) {
    this.name = name;
  }

  public void addSection(String name, ItemStack item, double chance) {
    if (dropSections.get(name) == null) {
      DropSection dropSection = new DropSection();
      dropSections.put(name, dropSection);
    }
    dropSections.get(name).addItem(item, chance);
  }

  public List<ItemStack> generateLoot(){
    List<ItemStack> items = new ArrayList<>();
    for (String sectionName : dropSections.keySet()) {
      ItemStack drop = dropSections.get(sectionName).getDrop();
      if (drop != null && drop.getType() != Material.AIR){
        items.add(drop);
      }
    }
    return items;
  }
}
