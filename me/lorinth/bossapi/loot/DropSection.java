package me.lorinth.bossapi.loot;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Random;
import org.bukkit.inventory.ItemStack;

public class DropSection {
  private HashMap<ItemStack, Double> drops = new HashMap<ItemStack, Double>();
  private Random random = new Random();

  void addItem(ItemStack item, double chance) {
    drops.put(item, chance);
  }

  ItemStack getDrop() {
    double roll = random.nextDouble();
    //System.out.println("Drops: " + drops);
    //System.out.println("Random roll: " + roll);
    for (Entry<ItemStack, Double> entry : drops.entrySet()) {
      roll -= entry.getValue();
      //System.out.println("roll... " + roll);
      if (roll <= 0) {
        //System.out.println("Dropping item " + entry.getKey().getItemMeta().getDisplayName());
        return entry.getKey();
      }
    }
    //System.out.println("No drop reached!");
    return null;
  }
}
