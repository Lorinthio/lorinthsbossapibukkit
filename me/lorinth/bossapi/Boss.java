package me.lorinth.bossapi;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;

import me.lorinth.bossapi.abilities.Ability;
import me.lorinth.bossapi.events.BossSpawnEvent;
import me.lorinth.bossapi.entities.*;
import net.minecraft.server.v1_12_R1.AttributeInstance;
import net.minecraft.server.v1_12_R1.Entity;
import net.minecraft.server.v1_12_R1.EntityInsentient;
import net.minecraft.server.v1_12_R1.GenericAttributes;
import net.minecraft.server.v1_12_R1.World;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.attribute.Attribute;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.inventory.ItemStack;

public class Boss {

	public EntityType type = EntityType.ZOMBIE;
	public String name;
	public double hp = 20;
	public double damage = 4;
	public double movespeed = 0.2;

	public String mountName = "";
	public boolean removeMountOnDeath = false;

	public int expReward = 0;

	double effectRadius = 2;
	double effectCount = 10;
	int effectData = 0;
	public Particle particle;

	public String key;

	public ItemStack held = new ItemStack(Material.AIR);
	public ItemStack off = new ItemStack(Material.AIR);
	public ItemStack helm = new ItemStack(Material.AIR);
	public ItemStack chest = new ItemStack(Material.AIR);
	public ItemStack legs = new ItemStack(Material.AIR);
	public ItemStack feet = new ItemStack(Material.AIR);

	public String lootTable = "";

	public HashMap<BossInstance.HealthSection, ArrayList<Ability>> onHit = new HashMap<BossInstance.HealthSection, ArrayList<Ability>>();
	public HashMap<BossInstance.HealthSection, ArrayList<Ability>> whenHit = new HashMap<BossInstance.HealthSection, ArrayList<Ability>>();
	public HashMap<BossInstance.HealthSection, ArrayList<Ability>> onEnter = new HashMap<BossInstance.HealthSection, ArrayList<Ability>>();

	public Boss(EntityType type, String key, String name, double hp, double damage, double movespeed){
		this.type = type;
		this.key = key;
		this.name = name;
		this.hp = hp;
		this.damage = damage;
		this.movespeed = movespeed;
	}

	private Class<? extends Entity> getBossClass(World world){
		switch(type){
			case BAT:
				return BossBat.class;
			case BLAZE:
				return BossBlaze.class;
			case CAVE_SPIDER:
				return BossCaveSpider.class;
			case CHICKEN:
				return BossChicken.class;
			case COW:
				return BossCow.class;
			case CREEPER:
				return BossCreeper.class;
			case DONKEY:
				return BossDonkey.class;
			case ELDER_GUARDIAN:
				return BossElderGuardian.class;
			case ENDER_DRAGON:
				return BossEnderDragon.class;
			case ENDERMAN:
				return BossEnderman.class;
			case ENDERMITE:
				return BossEndermite.class;
			case EVOKER:
				return BossEvoker.class;
			case GHAST:
				return BossGhast.class;
			case GIANT:
				return BossGiant.class;
			case GUARDIAN:
				return BossGuardian.class;
			case IRON_GOLEM:
				return BossIronGolem.class;
			case HORSE:
				return BossHorse.class;
			case HUSK:
				return BossHusk.class;
			case LLAMA:
				return BossLlama.class;
			case MAGMA_CUBE:
				return BossMagmaCube.class;
			case MULE:
				return BossMule.class;
			case MUSHROOM_COW:
				return BossMushroomCow.class;
			case OCELOT:
				return BossOcelot.class;
			case PIG:
				return BossPig.class;
			case PIG_ZOMBIE:
				return BossPigZombie.class;
			case POLAR_BEAR:
				return BossPolarBear.class;
			case RABBIT:
				return BossRabbit.class;
			case SHEEP:
				return BossSheep.class;
			case SHULKER:
				return BossShulker.class;
			case SILVERFISH:
				return BossSilverfish.class;
			case SKELETON:
				return BossSkeleton.class;
			case SKELETON_HORSE:
				return BossSkeletonHorse.class;
			case STRAY:
				return BossStray.class;
			case SLIME:
				return BossSlime.class;
			case SNOWMAN:
				return BossSnowman.class;
			case SPIDER:
				return BossSpider.class;
			case SQUID:
				return BossSquid.class;
			case VEX:
				return BossVex.class;
			case VILLAGER:
				return BossVillager.class;
			case VINDICATOR:
				return BossVindicator.class;
			case WITCH:
				return BossWitch.class;
			case WITHER:
				return BossWither.class;
			case WITHER_SKELETON:
				return BossWitherSkeleton.class;
			case WOLF:
				return BossWolf.class;
			case ZOMBIE:
				return BossZombie.class;
			case ZOMBIE_HORSE:
				return BossZombieHorse.class;
			default:
				System.out.println(new Exception("ERROR This entity type has not been handled in Boss.java/getBossClass()").toString());
				return BossZombie.class;
		}
	}

	public BossInstance spawn(Location loc) {
		World mcWorld = ((CraftWorld) loc.getWorld()).getHandle();
		EntityInsentient ent = null;
		try {
			Class<? extends Entity> c = getBossClass(mcWorld);
			Constructor<? extends Entity> con = c.getConstructor(new Class[]{World.class});
			ent = (EntityInsentient) con.newInstance(mcWorld);
			ent.setLocation(loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mcWorld.addEntity(ent, SpawnReason.CUSTOM);

		final LivingEntity entity = (LivingEntity) ent.getBukkitEntity();

		entity.teleport(loc);
		entity.getEquipment().setItemInMainHand(held);
		entity.getEquipment().setItemInMainHandDropChance(0);
		entity.getEquipment().setItemInOffHand(off);
		entity.getEquipment().setItemInOffHandDropChance(0);
		entity.getEquipment().setHelmet(helm);
		entity.getEquipment().setHelmetDropChance(0);
		entity.getEquipment().setChestplate(chest);
		entity.getEquipment().setChestplateDropChance(0);
		entity.getEquipment().setLeggings(legs);
		entity.getEquipment().setLeggingsDropChance(0);
		entity.getEquipment().setBoots(feet);
		entity.getEquipment().setBootsDropChance(0);

		AttributeInstance attribute = ent.getAttributeInstance(GenericAttributes.MOVEMENT_SPEED);
		if (attribute != null) {
			attribute.setValue(movespeed);
		}
		attribute = ent.getAttributeInstance(GenericAttributes.ATTACK_DAMAGE);
		if (attribute != null) {
			attribute.setValue(damage);
		}
		attribute = ent.getAttributeInstance(GenericAttributes.FOLLOW_RANGE);
		if (attribute != null) {
			attribute.setValue(30);
		}
		ent.goalSelector.a();
		ent.setCustomName(name);
		ent.setCustomNameVisible(true);

		entity.setCustomName(name);
		entity.setRemoveWhenFarAway(true);
		entity.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(hp);
		entity.setHealth(hp);

		BossInstance bi = new BossInstance(this, entity);
		bi.onHit = onHit;
		bi.whenHit = whenHit;
		bi.onEnter = onEnter;

		try{
			if (!mountName.equalsIgnoreCase("")) {
				BossInstance mount = BossApi.getPlugin().bossNames.get(mountName).spawn(loc);
				mount.bossEntity.addPassenger(entity);
				bi.mount = mount;
			}
		}
		catch(NullPointerException e){

		}

		if(particle != null){
			bi.effectCount = effectCount;
			bi.effectRadius = effectRadius;
			bi.particle = particle;
			bi.StartParticles();
		}

		BossSpawnEvent event = new BossSpawnEvent(bi, entity);
		Bukkit.getPluginManager().callEvent(event);

		return bi;
	}

	/* On Hit
	 * - When boss hits, chance to cast
	 *
	 * When Hit
	 * - When boss is hit, chance to cast
	 *
	 * On Enter new phase
	 * - When boss enters new phase, casts everytime

	*/

}
