package me.lorinth.bossapi;

import java.io.File;
import java.io.IOException;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class Spawner {

  private BossInstance bossInstance;
  private long respawnDelay;
  private long bossSpawnTime;
  private int resetCount = 0;
  private double maxDistance;
  private Location loc;
  private Chunk chunk;

  public Boss boss;

  public Spawner(Boss boss, Location loc, long delay, double distance) {
    this.boss = boss;
    this.loc = loc;
    this.respawnDelay = delay;
    this.maxDistance = distance;
    this.bossSpawnTime = System.currentTimeMillis();
    this.chunk = loc.getChunk();
  }

  public void save(String key) {
    File spawnersFile = new File(BossApi.api.getDataFolder(), "Spawners.yml");
    FileConfiguration spawners = new YamlConfiguration();

    try {
      spawners.load(spawnersFile);
    } catch (IOException | InvalidConfigurationException e1) {
      // TODO Auto-generated catch block
      System.out.println("Cannot find file Spawners.yml");

      e1.printStackTrace();
    }

    spawners.set(key + ".Location.world", this.loc.getWorld().getName());
    spawners.set(key + ".Location.x", Integer.valueOf(this.loc.getBlockX()));
    spawners.set(key + ".Location.y", Integer.valueOf(this.loc.getBlockY()));
    spawners.set(key + ".Location.z", Integer.valueOf(this.loc.getBlockZ()));
    spawners.set(key + ".Boss", this.boss.key);
    spawners.set(key + ".RespawnDelay", Long.valueOf(this.respawnDelay));
    spawners.set(key + ".MaxDistance", Double.valueOf(this.maxDistance));

    try {
      spawners.save(spawnersFile);
    }
    catch (IOException e) {
      System.out.println("Failed to save spawner...");
      e.printStackTrace();
    }
  }

  public BossInstance getBossInstance() {
    return this.bossInstance;
  }

  public void setBossInstance(BossInstance bossInstance) {
    this.bossInstance = bossInstance;
  }

  public Location getLocation() {
    return this.loc;
  }

  public double getRadius() {
    return this.maxDistance;
  }

  public void setResetCount(int amount) {
    if (amount >= 3) {
      killBoss();
      spawnBoss();
      amount = 0;
    }
    this.resetCount = amount;
  }

  public int getResetCount() {
    return  this.resetCount;
  }

  public long getBossSpawnTime() {
    return this.bossSpawnTime;
  }

  public Chunk getChunk() {
    return this.chunk;
  }

  public boolean isInChunk(Chunk chunk) {
    return this.loc.getChunk() == chunk;
  }

  public void spawnBoss() {
    if (this.bossInstance != null) {
      return;
    }
    this.bossInstance = this.boss.spawn(this.loc);
    this.bossInstance.spawner = this;
  }

  public void killBoss() {
    if (this.bossInstance == null) {
      return;
    }
    if (this.bossInstance.mount != null) {
      this.bossInstance.mount.bossEntity.remove();
    }
    this.bossInstance.bossEntity.remove();
    BossApi.api.bossIds.remove(this.bossInstance.getBossEntity().getEntityId());
    this.bossInstance = null;
  }

  public void bossDied() {
    this.bossSpawnTime = System.currentTimeMillis() + respawnDelay * 1000;
    if (BossApi.api.bossIds.containsKey(this.bossInstance.bossEntity.getEntityId())) {
      BossApi.api.bossIds.remove(this.bossInstance.bossEntity.getEntityId());
    }
    this.bossInstance = null;
  }
}