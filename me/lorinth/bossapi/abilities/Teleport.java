package me.lorinth.bossapi.abilities;

import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;

public class Teleport extends Action{

	Location loc;
	
	public Teleport(Location loc){
		this.loc = loc;
	}
	
	@Override
	public void execute(LivingEntity ent){
		//Entity doesn't change worlds
		loc.setWorld(ent.getWorld());
		ent.teleport(loc);
	}
	
}
