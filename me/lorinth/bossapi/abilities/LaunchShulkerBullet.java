package me.lorinth.bossapi.abilities;

import me.lorinth.bossapi.BossApi;
import org.bukkit.entity.Creature;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.ShulkerBullet;
import org.bukkit.util.Vector;

public class LaunchShulkerBullet extends Action{

	double damage;
	TargetType type;
	double radius;

	public LaunchShulkerBullet(TargetType type, double damage){
		this.type = type;
		this.damage = damage;
	}

	public LaunchShulkerBullet(TargetType type, double damage, double radius){
		this.type = type;
		this.damage = damage;
		this.radius = radius;
	}
	
	@Override
	public void execute(LivingEntity ent){
		
		if(type == TargetType.Target){
			ShulkerBullet bullet = ent.getWorld().spawn(ent.getEyeLocation(), ShulkerBullet.class);
			bullet.setShooter(ent);
			bullet.setTarget(getTarget(ent));
			
			BossApi.getPlugin().projectiles.put(bullet, (int) damage);
		}
		else if(type == TargetType.AoeCreatures){
			for(Creature c : this.getNearbyCreatures(radius, ent)){
                ShulkerBullet bullet = ent.getWorld().spawn(ent.getEyeLocation(), ShulkerBullet.class);
                bullet.setVelocity(new Vector(0, 0, 0));
                bullet.setShooter(ent);
                bullet.setTarget(c);
				
				BossApi.getPlugin().projectiles.put(bullet, (int) damage);
			}
		}
		else if(type == TargetType.AoePlayers){
			for(Player p : this.getNearbyPlayers(radius, ent)){
                ShulkerBullet bullet = ent.getWorld().spawn(ent.getEyeLocation(), ShulkerBullet.class);
                bullet.setVelocity(new Vector(0, 0, 0));
                bullet.setShooter(ent);
                bullet.setTarget(p);

                BossApi.getPlugin().projectiles.put(bullet, (int) damage);
			}
		}
	}
	
}
