package me.lorinth.bossapi.abilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import me.lorinth.bossapi.BossApi;

import org.bukkit.Bukkit;
import org.bukkit.entity.Creature;
import org.bukkit.entity.LivingEntity;
import org.bukkit.scheduler.BukkitRunnable;

public class AbilityTask extends BukkitRunnable {

	private final Creature caster;
	private List<Action> actions;

	private HashMap<Integer, List<Action>> actionsAtDelay = new HashMap<>();
	private boolean hasDelay = false;

    AbilityTask(List<Action> actions2, LivingEntity ent){
		this.actions = actions2;

		caster = (Creature) ent;
		checkDelays();
	}

	private void checkDelays() {
		hasDelay = false;
		for (Action a : actions) {
			if (a instanceof Wait) {
				hasDelay = true;
			}
		}

		if (hasDelay) {
			double delay = 0;
			double newDelay = 0;
			List<Action> actionSet = new ArrayList<>();
			for(Action a : actions){
				if (a instanceof Wait) {
					newDelay += ((Wait) a).delay * 20;
					if(newDelay != delay){
						actionsAtDelay.put((int) delay, actionSet);
						actionSet = new ArrayList<>();
					}
					delay = newDelay;
				}
				else{
					actionSet.add(a);
				}
			}
			actionsAtDelay.put((int) delay, actionSet);
		}
	}

	@Override
	public void run() {
		if (caster.isDead()) {
			return;
		}
		if (hasDelay) {
			for (final Integer time : actionsAtDelay.keySet()) {
				if (time == 0) {
					for (Action a : actionsAtDelay.get(time)) {
						a.execute(caster);
					}
				} else {
					Bukkit.getScheduler().scheduleSyncDelayedTask(BossApi.getPlugin(), new Runnable(){
						@Override
						public void run() {
							if (caster.isDead()) {
								return;
							}
							for (Action a : actionsAtDelay.get(time)) {
								a.execute(caster);
							}
						}
					}, time);
				}
			}
		} else {
			for (Action a : actions) {
				a.execute(caster);
			}
		}
	}
}
