package me.lorinth.bossapi.abilities;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.LivingEntity;

public class Ability extends Action {
	
	public double chance;
	public double cooldown;
	List<Action> actions = new ArrayList<Action>();
	public String name;

	public Ability(double chance, String name, List<Action> actions, double cooldown){
		this.chance = chance;
		this.name = name;
		this.cooldown = cooldown;
		this.actions = actions;
	}
	
	public Ability(String name, List<Action> actions, double cooldown){
		chance = 10;
		this.name = name;
		this.cooldown = cooldown;
		this.actions = actions;
	}
	
	public void Cast(final LivingEntity ent){
		AbilityTask task = new AbilityTask(actions, ent);
		task.run();
	}
	
	public Ability clone(){
		return new Ability(chance, name, actions, cooldown);
	}

}
