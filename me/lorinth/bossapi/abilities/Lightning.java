package me.lorinth.bossapi.abilities;

import org.bukkit.Sound;
import org.bukkit.entity.Creature;
import org.bukkit.entity.LightningStrike;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class Lightning extends Action{

	TargetType type;
	double damage;
	double radius;
	
	public Lightning(TargetType type, double damage){
		this.type = type;
		this.damage = damage;
	}
	
	public Lightning(TargetType type, double damage, double radius){
		this.type = type;
		this.damage = damage;
		this.radius = radius;
	}
	
	@Override
	public void execute(LivingEntity ent){
		if(type == TargetType.Target){
			LivingEntity targ = getTarget(ent);
			LightningStrike ls = targ.getWorld().strikeLightningEffect(targ.getLocation());
			ls.setSilent(true);
            targ.getWorld().playSound(targ.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
			if(damage > 0){
				targ.damage(damage);
			}
		}
		else if(type == TargetType.Self){
			LightningStrike ls = ent.getWorld().strikeLightningEffect(ent.getLocation());
			ls.setSilent(true);
            ent.getWorld().playSound(ent.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
			if(damage > 0){
				ent.damage(damage);
			}
		}
		else if(type == TargetType.AoePlayers){
			for(Player p : getNearbyPlayers(radius, ent)){
				LightningStrike ls = p.getWorld().strikeLightningEffect(p.getLocation());
				ls.setSilent(true);
                p.getWorld().playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
				if(damage > 0){
					p.damage(damage);
				}
			}
		}
		else if(type == TargetType.AoeCreatures){
			for(Creature c : getNearbyCreatures(radius, ent)){
				LightningStrike ls = c.getWorld().strikeLightningEffect(c.getLocation());
				ls.setSilent(true);
                c.getWorld().playSound(c.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
				if(damage > 0){
					c.damage(damage);
				}
			}
		}
	}
	
}
