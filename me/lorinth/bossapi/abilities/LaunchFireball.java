package me.lorinth.bossapi.abilities;

import me.lorinth.bossapi.BossApi;

import org.bukkit.Sound;
import org.bukkit.entity.*;
import org.bukkit.util.Vector;

public class LaunchFireball extends Action{

	double damage;
	double speed;
	TargetType type;
	double radius;
	
	public LaunchFireball(TargetType type, double damage, double speed){
		this.type = type;
		this.damage = damage;
		this.speed = speed;
	}
	
	public LaunchFireball(TargetType type, double damage, double speed, double radius){
		this.type = type;
		this.damage = damage;
		this.speed = speed;
		this.radius = radius;
	}
	
	@Override
	public void execute(LivingEntity ent){
		//System.out.println("Fireball has target, " + type.toString());
		
		if(type == TargetType.Target){
            ent.getWorld().playSound(ent.getLocation(), Sound.ENTITY_BLAZE_SHOOT, 1.0f, 1.0f);
			SmallFireball ball = ent.launchProjectile(SmallFireball.class);
			ball.setIsIncendiary(false);
			ball.setShooter(ent);
			ball.setVelocity(ent.getEyeLocation().getDirection().multiply(speed));
			
			BossApi.getPlugin().projectiles.put(ball, (int) damage);
		}
		else if(type == TargetType.AoeCreatures){
            ent.getWorld().playSound(ent.getLocation(), Sound.ENTITY_BLAZE_SHOOT, 1.0f, 2.0f);
			for(Creature c : this.getNearbyCreatures(radius, ent)){
				SmallFireball ball = ent.launchProjectile(SmallFireball.class);
				ball.setIsIncendiary(false);
				ball.setShooter(ent);
				ball.setVelocity(getVelocity(ent, c).multiply(speed));
				
				BossApi.getPlugin().projectiles.put(ball, (int) damage);
			}
		}
		else if(type == TargetType.AoePlayers){
		    ent.getWorld().playSound(ent.getLocation(), Sound.ENTITY_BLAZE_SHOOT, 1.0f, 2.0f);
			for(Player p : this.getNearbyPlayers(radius, ent)){
				SmallFireball ball = ent.launchProjectile(SmallFireball.class);
				ball.setIsIncendiary(false);
				ball.setShooter(ent);
				ball.setVelocity(getVelocity(ent, p).multiply(speed));
				
				BossApi.getPlugin().projectiles.put(ball, (int) damage);
			}
		}

	}
	
	private Vector getVelocity(Entity from, Entity to){
	    Vector p1 = to.getLocation().toVector().subtract(from.getLocation().toVector());
        return p1.normalize();
	}
	
}
