package me.lorinth.bossapi.abilities;

public enum TargetEntity {
	Player, Creature, All
}
