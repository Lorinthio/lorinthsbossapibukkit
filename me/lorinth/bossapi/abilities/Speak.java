package me.lorinth.bossapi.abilities;

import org.bukkit.Bukkit;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class Speak extends Action{

	TargetType type;
	String message;
	double radius;
	
	public Speak(String line, TargetType type){
		this.type = type;
		message = line;
	}
	
	public Speak(String line, double radius){
		type = TargetType.AoePlayers;
		message = line;
		this.radius = radius;
	}
	
	@Override
	public void execute(LivingEntity ent){
		if(type == TargetType.Target){
			LivingEntity target = getTarget(ent);
			if(target instanceof Player){
				target.sendMessage(message);
			}
		}
		else if(type == TargetType.AoePlayers){
			for(Player p : getNearbyPlayers(radius, ent)){
				p.sendMessage(message);
			}
		}
		else if(type == TargetType.Self){
			Bukkit.getConsoleSender().sendMessage(message);
		}
	}
	
}
