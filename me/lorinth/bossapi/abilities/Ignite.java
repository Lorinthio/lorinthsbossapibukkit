package me.lorinth.bossapi.abilities;

import org.bukkit.entity.Creature;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class Ignite extends Action{

	TargetType type;
	double duration;
	double radius;
	
	public Ignite(TargetType type, double duration){
		this.type = type;
		this.duration = duration;
	}
	
	public Ignite(TargetType type, double radius, double duration){
		this.type = type;
		this.radius = radius;
		this.duration = duration;
	}
	
	@Override
	public void execute(LivingEntity ent){
		if(type == TargetType.Target){
			LivingEntity living = getTarget(ent);
			living.setFireTicks((int) (duration * 20));
		}
		else if(type == TargetType.AoePlayers){
			for(Player p : getNearbyPlayers(radius, ent)){
				p.setFireTicks((int) (duration * 20));
			}
		}
		else if(type == TargetType.AoeCreatures){
			for(Creature c : getNearbyCreatures(radius, ent)){
				c.setFireTicks((int) (duration * 20));
			}
		}
		else if(type == TargetType.Self){
			ent.setFireTicks((int) (duration * 20));
		}
	}
	
}
