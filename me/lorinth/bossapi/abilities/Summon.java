package me.lorinth.bossapi.abilities;

import me.lorinth.bossapi.Boss;
import me.lorinth.bossapi.BossApi;

import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;

public class Summon extends Action{

	EntityType type;
	Integer count;
	double radius;
	
	Boss b;
	String bossname;
	
	public Summon(EntityType type, Integer number){
		this.type = type;
		count = number;
	}
	
	public Summon(String bossname, Integer count){
		this.bossname = bossname;
		this.count = count;
	}

	@Override
	public void execute(LivingEntity ent){
		World w = ent.getWorld();
		for(int i=0; i<count; i++){
			if(type != null){
				w.spawnEntity(ent.getLocation(), type);
			}
			else{
				if(b == null){
					b = BossApi.getPlugin().bossNames.get(bossname);
				}
				//System.out.println("Trying to spawn, " + boss.name);
				b.spawn(ent.getLocation());
			}
		}
	}
	
}
