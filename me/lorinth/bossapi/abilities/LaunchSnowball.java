package me.lorinth.bossapi.abilities;

import me.lorinth.bossapi.BossApi;

import org.bukkit.entity.*;
import org.bukkit.util.Vector;

public class LaunchSnowball extends Action{

	double damage;
	double speed;
	TargetType type;
	double radius;
	
	public LaunchSnowball(TargetType type, double damage, double speed){
		this.type = type;
		this.damage = damage;
		this.speed = speed;
	}
	
	public LaunchSnowball(TargetType type, double damage, double speed, double radius){
		this.type = type;
		this.damage = damage;
		this.speed = speed;
		this.radius = radius;
	}
	
	@Override
	public void execute(LivingEntity ent){
		if(type == TargetType.Target){
			final Snowball ball = ent.launchProjectile(Snowball.class);
			ball.setShooter(ent);
			ball.setVelocity(ent.getEyeLocation().getDirection().multiply(speed));
			
			BossApi.getPlugin().projectiles.put(ball, (int) damage);
		}
		else if(type == TargetType.AoeCreatures){
			for(Creature c : this.getNearbyCreatures(radius, ent)){
				final Snowball ball = ent.launchProjectile(Snowball.class);
				ball.setShooter(ent);
				ball.setVelocity(getVelocity(ent, c).multiply(speed));
				
				BossApi.getPlugin().projectiles.put(ball, (int) damage);
			}
		}
		else if(type == TargetType.AoePlayers){
			for(Player p : this.getNearbyPlayers(radius, ent)){
				final Snowball ball = ent.launchProjectile(Snowball.class);
				ball.setShooter(ent);
				ball.setVelocity(getVelocity(ent, p).multiply(speed));
				
				BossApi.getPlugin().projectiles.put(ball, (int) damage);
			}
		}
	}

    private Vector getVelocity(Entity from, Entity to){
        Vector p1 = to.getLocation().toVector().subtract(from.getLocation().toVector());
        return p1.normalize();
    }
	
}
