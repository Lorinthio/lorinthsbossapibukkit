package me.lorinth.bossapi.abilities;

public enum TargetType {
	Self, Target, AoeCreatures, AoePlayers;
}
