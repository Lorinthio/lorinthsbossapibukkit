package me.lorinth.bossapi.abilities;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.GameMode;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class Action {

	public void execute(LivingEntity ent){

	}
	
	public List<Player> getNearbyPlayers(double radius, LivingEntity ent){
		List<Player> players = new ArrayList<Player>();
		for(Entity e : ent.getNearbyEntities(radius, radius, radius)){
			if(e instanceof Player){
				if(((Player)e).getGameMode() == GameMode.SURVIVAL){
					players.add((Player) e);
				}
			}
		}
		return players;
	}
	
	public List<Creature> getNearbyCreatures(double radius, LivingEntity ent){
		List<Creature> creatures = new ArrayList<Creature>();
		for(Entity e : ent.getNearbyEntities(radius, radius, radius)){
			if(e instanceof Creature){
				creatures.add((Creature) e);
			}
		}
		creatures.remove(ent);
		return creatures;
	}

	public LivingEntity getTarget(LivingEntity livingEntity) {
		if(livingEntity instanceof Creature) {
			Creature creature = (Creature) livingEntity;
			return creature.getTarget();
		} else {
			for(Player player : getNearbyPlayers(10, livingEntity)) {
				if(livingEntity.hasLineOfSight(player)) {
					return player;
				}
			}
		}
		return null;
	}

}
