package me.lorinth.bossapi.abilities;

import me.lorinth.bossapi.BossApi;
import org.bukkit.Sound;
import org.bukkit.entity.*;
import org.bukkit.util.Vector;

public class LaunchWitherSkull extends Action{

	double damage;
	double speed;
	TargetType type;
	double radius;

	public LaunchWitherSkull(TargetType type, double damage, double speed){
		this.type = type;
		this.damage = damage;
		this.speed = speed;
	}

	public LaunchWitherSkull(TargetType type, double damage, double speed, double radius){
		this.type = type;
		this.damage = damage;
		this.speed = speed;
		this.radius = radius;
	}
	
	@Override
	public void execute(LivingEntity ent){
		//System.out.println("Skull has target, " + type.toString());
		
		if(type == TargetType.Target){
            ent.getWorld().playSound(ent.getLocation(), Sound.ENTITY_WITHER_SHOOT, 1.0f, 1.0f);
			WitherSkull skull = ent.launchProjectile(WitherSkull.class);
			skull.setShooter(ent);
			skull.setVelocity(ent.getEyeLocation().getDirection().multiply(speed));
			
			BossApi.getPlugin().projectiles.put(skull, (int) damage);
		}
		else if(type == TargetType.AoeCreatures){
            ent.getWorld().playSound(ent.getLocation(), Sound.ENTITY_WITHER_SHOOT, 1.0f, 2.0f);
			for(Creature c : this.getNearbyCreatures(radius, ent)){
				WitherSkull skull = ent.launchProjectile(WitherSkull.class);
				skull.setShooter(ent);
				skull.setVelocity(getVelocity(ent, c).multiply(speed));
				
				BossApi.getPlugin().projectiles.put(skull, (int) damage);
			}
		}
		else if(type == TargetType.AoePlayers){
		    ent.getWorld().playSound(ent.getLocation(), Sound.ENTITY_WITHER_SHOOT, 1.0f, 2.0f);
			for(Player p : this.getNearbyPlayers(radius, ent)){
				WitherSkull skull = ent.launchProjectile(WitherSkull.class);
				skull.setShooter(ent);
				skull.setVelocity(getVelocity(ent, p).multiply(speed));
				
				BossApi.getPlugin().projectiles.put(skull, (int) damage);
			}
		}

	}
	
	private Vector getVelocity(Entity from, Entity to){
	    Vector p1 = to.getLocation().toVector().subtract(from.getLocation().toVector());
        return p1.normalize();
	}
	
}
