package me.lorinth.bossapi.abilities;

import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class Knockback extends Action{

	TargetType type;
	double power;
	double radius;
	
	//Single target
	public Knockback(double power){
		this.type = TargetType.Target;
		this.power = power;
	}
	
	public Knockback(TargetType type, double radius, double power){
		this.radius = radius;
		this.type = type;
		this.power = power;
	}
	
	@Override
	public void execute(LivingEntity ent){
		if(type == TargetType.Target){
			LivingEntity targ = getTarget(ent);
			targ.setVelocity(getVelocity(ent, targ).multiply(power));
		}
		else if(type == TargetType.AoePlayers){
			for(Player p : getNearbyPlayers(radius, ent)){
				p.setVelocity(getVelocity(ent, p).multiply(power));
			}
		}
		else if(type == TargetType.AoeCreatures){
			for(Creature c : getNearbyCreatures(radius, ent)){
				c.setVelocity(getVelocity(ent, c).multiply(power));
			}
		}
	}

	private Vector getVelocity(Entity from, Entity to){
		Vector p1 = to.getLocation().toVector().subtract(from.getLocation().toVector());
		return p1.normalize();
	}
	
}
