package me.lorinth.bossapi.entities;

import net.minecraft.server.v1_12_R1.EntityZombieHusk;
import net.minecraft.server.v1_12_R1.World;

/**
 * Created by Ben on 12/11/2016.
 */
public class BossHusk extends EntityZombieHusk{
    public BossHusk(World w){
        super(w);
    }
}
