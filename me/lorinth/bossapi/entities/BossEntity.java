package me.lorinth.bossapi.entities;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.craftbukkit.v1_12_R1.entity.CraftEntity;
import org.bukkit.entity.Entity;

public class BossEntity {

	private static List<Class<?>> types = new ArrayList<Class<?>>();
	
	public static void addType(Class<?> newClass){
		types.add(newClass);
	}
	
	public static boolean isBossEntity(Entity ent){
		net.minecraft.server.v1_12_R1.Entity ent2 = ((CraftEntity) ent).getHandle();
		return types.contains(ent2.getClass());
	}
}


