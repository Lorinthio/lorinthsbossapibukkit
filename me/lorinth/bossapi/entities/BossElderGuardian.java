package me.lorinth.bossapi.entities;

import net.minecraft.server.v1_12_R1.EntityGuardianElder;
import net.minecraft.server.v1_12_R1.World;

/**
 * Created by Ben on 12/11/2016.
 */
public class BossElderGuardian extends EntityGuardianElder {
    public BossElderGuardian(World w){
        super(w);
    }
}
