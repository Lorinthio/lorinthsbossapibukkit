package me.lorinth.bossapi.entities;

import net.minecraft.server.v1_12_R1.EntitySkeletonStray;
import net.minecraft.server.v1_12_R1.World;

/**
 * Created by Ben on 12/11/2016.
 */
public class BossStray extends EntitySkeletonStray {
    public BossStray(World w){
        super(w);
    }
}
