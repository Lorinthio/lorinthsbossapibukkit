package me.lorinth.bossapi.entities;

import com.google.common.collect.Sets;
import me.lorinth.bossapi.pathfinders.PathfinderGoalAnimalMeleeAttack;
import net.minecraft.server.v1_12_R1.*;

import java.lang.reflect.Field;

/**
 * Created by Ben on 11/23/2016.
 */
public class BossIronGolem extends EntityIronGolem{

    public BossIronGolem(World world) {
        super(world);
        try {
            Field bField = PathfinderGoalSelector.class.getDeclaredField("b");
            bField.setAccessible(true);
            Field cField = PathfinderGoalSelector.class.getDeclaredField("c");
            cField.setAccessible(true);
            bField.set(this.goalSelector, Sets.newLinkedHashSet());
            bField.set(this.targetSelector, Sets.newLinkedHashSet());
            cField.set(this.goalSelector, Sets.newLinkedHashSet());
            cField.set(this.targetSelector, Sets.newLinkedHashSet());
        }
        catch (Exception exc) {
            exc.printStackTrace();
        }

        this.goalSelector.a(0, new PathfinderGoalFloat(this));
        this.goalSelector.a(1, new PathfinderGoalAnimalMeleeAttack(this, 1.0D, false));
        this.goalSelector.a(2, new PathfinderGoalRandomLookaround(this));
        this.goalSelector.a(3, new PathfinderGoalRandomStroll(this, 1.0D));
        this.targetSelector.a(1, new PathfinderGoalHurtByTarget(this, true));
        this.targetSelector.a(2, new PathfinderGoalNearestAttackableTarget(this, EntityLiving.class, true));
    }

}
