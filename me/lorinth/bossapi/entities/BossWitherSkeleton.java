package me.lorinth.bossapi.entities;

import net.minecraft.server.v1_12_R1.EntitySkeletonWither;
import net.minecraft.server.v1_12_R1.World;

/**
 * Created by Ben on 12/11/2016.
 */
public class BossWitherSkeleton extends EntitySkeletonWither{
    public BossWitherSkeleton(World w){
        super(w);
    }
}
