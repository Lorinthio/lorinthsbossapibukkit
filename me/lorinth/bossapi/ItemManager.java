package me.lorinth.bossapi;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;

import me.lorinth.bossapi.events.BossDeathEvent;
import me.lorinth.bossapi.loot.LootTable;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

class ItemManager {

	private HashMap<String, ItemStack> itemList = new HashMap<String, ItemStack>();
	private HashMap<String, LootTable> tables = new HashMap<String, LootTable>();

	private File itemsFile;
	private FileConfiguration items;

	private File lootTableFile;
	private FileConfiguration lootTables;

	ItemManager(){
		LoadData();
	}

	private void LoadData(){
		itemsFile = new File(BossApi.api.getDataFolder(), "Items.yml");
		lootTableFile = new File(BossApi.api.getDataFolder(), "LootTables.yml");

		try {
			firstRun();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		items = new YamlConfiguration();
		lootTables = new YamlConfiguration();

		loadYamls();
		loadItems();
		loadTables();
	}

	private void loadItems(){
		for (String key : items.getConfigurationSection("").getKeys(false)) {
			ItemStack item;
			Material material = Material.getMaterial(items.getString(key + ".Id"));
			Integer d = items.getInt(key + ".Data");
			String name = items.getString(key + ".Display");
			List<String> lore = items.getStringList(key + ".Lore");
			List<String> flags = items.getStringList(key + ".Flags");

			item = new ItemStack(material);
			item.setDurability((short)(int)d);

			if (material != Material.POTION) {
				ItemMeta meta = item.getItemMeta();
				meta.setDisplayName(BossApi.api.convertToMColors(name));
				meta.setLore(BossApi.api.convertToMColors(lore));
				for (String s : flags) {
					if (s.equalsIgnoreCase("unbreakable")) {
						meta.setUnbreakable(true);
						continue;
					}
					ItemFlag itemFlag;
					try {
						itemFlag = ItemFlag.valueOf(s);
					} catch (NullPointerException e) {
						System.out.println("ERROR! Invalid flag in item " + key);
						continue;
					}
					meta.addItemFlags(itemFlag);
				}
				item.setItemMeta(meta);
			}
			else{
				PotionMeta meta = (PotionMeta) item.getItemMeta();
				meta.setDisplayName(BossApi.api.convertToMColors(name));
				meta.setLore(BossApi.api.convertToMColors(lore));

				for(String effect : items.getStringList(key + ".PotionEffects")){
					String[] values = effect.split(" ");
					try{
						PotionEffectType type = PotionEffectType.getByName(values[0]);
						Integer duration = Integer.parseInt(values[1]);
						Integer potency = Integer.parseInt(values[2]);

						meta.addCustomEffect(new PotionEffect(type, duration, potency), true);
					}
					catch(Exception e){
						System.out.println("ERROR! Invalid potion effect for item, " + key);
						System.out.println("Effect : " + effect);
					}
				}

				item.setItemMeta(meta);
			}
			itemList.put(key, item);
		}
	}

	private void loadTables(){
		for (String key : lootTables.getConfigurationSection("").getKeys(false)) {
			System.out.println("Creating loot table named " + key);
			tables.put(key, createLootTable(key));
		}
	}

	private LootTable createLootTable(String key) {
		LootTable table = new LootTable(key);
		for (String catagoryKey : lootTables.getConfigurationSection(key).getKeys(false)) {
			System.out.println("-Catagory " + catagoryKey);
			for (String itemKey : lootTables.getConfigurationSection(key + "." + catagoryKey).getKeys(false)) {
				double chance = lootTables.getDouble(key + "." + catagoryKey + "." + itemKey);
				table.addSection(catagoryKey, getItem(itemKey), chance);
				System.out.println("--Added " + itemKey + " with chance " + chance);
			}
		}
		return table;
	}

	ItemStack getItem(String key){
		try {
			return itemList.get(key);
		} catch (NullPointerException e) {
			System.out.println("ERROR! Item with key " + key + "NOT FOUND!");
			return null;
		}
	}

	private void loadYamls(){
		try{
			items.load(itemsFile);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		try{
			lootTables.load(lootTableFile);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	private void firstRun() throws Exception {
		if(!itemsFile.exists()){                        // checks if the yaml does not exists
			itemsFile.getParentFile().mkdirs();         // creates the /plugins/<pluginName>/ directory if not found
			copy(BossApi.api.getResource("Items.yml"), itemsFile); // copies the yaml from your jar to the folder /plugin/<pluginName>
		}
		if(!lootTableFile.exists()){
			lootTableFile.getParentFile().mkdirs();
			copy(BossApi.api.getResource("LootTables.yml"), lootTableFile);
		}
	}

	private void copy(InputStream in, File file) {
		try {
			OutputStream out = new FileOutputStream(file);
			byte[] buf = new byte[1024];
			int len;
			while((len=in.read(buf))>0){
				out.write(buf,0,len);
			}
			out.close();
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	void dropItems(BossDeathEvent e) {
		String tableName = e.getBossInstance().boss.lootTable;
		if (tableName == null || tableName.equalsIgnoreCase("") || !tables.containsKey(tableName)) {
			System.out.println("ERROR! Table " + tableName + " does not exist!");
			return;
		}
		LootTable lootTable = tables.get(tableName);
		for (ItemStack item : lootTable.generateLoot()) {
			item.setAmount(1);
			e.getWorld().dropItemNaturally(e.getLocation(), item.clone());
			System.out.println("Dropping item: " + item.getItemMeta().getDisplayName());
		}
	}
}
