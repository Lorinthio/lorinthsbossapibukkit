package me.lorinth.bossapi;

import me.lorinth.bossapi.entities.BossEntity;
import net.minecraft.server.v1_12_R1.EntityInsentient;
import net.minecraft.server.v1_12_R1.EntityTypes;

import net.minecraft.server.v1_12_R1.MinecraftKey;

public class NMSUtils {
    public void registerEntity(String name, int id, Class<? extends EntityInsentient> customClass) {
        try {
 
            /*
            * First, we make a list of all HashMap's in the EntityTypes class
            * by looping through all fields. I am using reflection here so we
            * have no problems later when minecraft changes the field's name.
            * By creating a list of these maps we can easily modify them later
            * on.
            */
        	BossEntity.addType(customClass);
            EntityTypes.b.a(id, new MinecraftKey(name), customClass);
 
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
