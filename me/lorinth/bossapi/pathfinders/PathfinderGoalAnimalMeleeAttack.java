package me.lorinth.bossapi.pathfinders;

import net.minecraft.server.v1_12_R1.*;

/**
 * Created by Ben on 11/23/2016.
 */
public class PathfinderGoalAnimalMeleeAttack extends PathfinderGoalMeleeAttack{
    private final EntityCreature h;
    private int i;
    private double maxDist;

    public PathfinderGoalAnimalMeleeAttack(EntityCreature creature, double maxDist, double paramDouble, boolean paramBoolean)
    {
        super(creature, paramDouble, paramBoolean);
        this.h = creature;
        this.maxDist = maxDist;
    }

    public PathfinderGoalAnimalMeleeAttack(EntityCreature creature, double paramDouble, boolean paramBoolean)
    {
        super(creature, paramDouble, paramBoolean);
        this.h = creature;
        this.maxDist = 1.2;
    }

    public void attack(Entity entity){
        if (entity != null) {
            if(getDistance(entity) < maxDist){
                entity.damageEntity(DamageSource.mobAttack(h), 3.0F);
            }
        }
    }

    private double getDistance(Entity entity){
        double distance = 0;
        double x1 = h.locX;
        double x2 = entity.locX;
        double y1 = h.locY;
        double y2 = entity.locY;
        double z1 = h.locZ;
        double z2 = entity.locZ;
        distance += Math.abs(x1 - x2);
        distance += Math.abs(y1 - y2);
        distance += Math.abs(z1 - z2);

        return distance;
    }

    //Not sure, seems like a reset/clear
    public void c()
    {
        super.c();
        this.i = 0;
    }

    //Triggers when creature is close enough to attack target
    public void d() {
        super.d();
        EntityLiving ent = this.h.getGoalTarget();
        attack(ent);
    }
}
